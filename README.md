# nginx for ubuntu

## - change your own IP address in inventory.cfg file
## - run the following command as root:
	 ansible-playbook -i inventory.cfg nginx_install.yml -b 


## - you should see something like that:

PLAY [web] *********************************************************************

TASK [Gathering Facts] *********************************************************
ok: [192.168.1.103]

TASK [ensure nginx is at the latest version] ***********************************
ok: [192.168.1.103]

TASK [start nginx] *************************************************************
ok: [192.168.1.103]

PLAY RECAP *********************************************************************
192.168.1.103              : ok=3    changed=0    unreachable=0    failed=0    skipped=0    rescued=0    ignored=0   



## - run the following command as root
		ansible-playbook -i inventory.cfg  --limit 192.168.56.11 nginx.yml
## - you should see something like that:

PLAY [web] *********************************************************************

TASK [Gathering Facts] *********************************************************
ok: [192.168.1.103]

TASK [ensure nginx is at the latest version] ***********************************
ok: [192.168.1.103]

TASK [start nginx] *************************************************************
ok: [192.168.1.103]

TASK [copy the nginx config file and restart nginx] ****************************
ok: [192.168.1.103]

TASK [create symlink] **********************************************************
ok: [192.168.1.103]

TASK [copy the content of the web site] ****************************************
ok: [192.168.1.103]

TASK [restart nginx] ***********************************************************
changed: [192.168.1.103]

PLAY RECAP *********************************************************************
192.168.1.103              : ok=7    changed=1    unreachable=0    failed=0    skipped=0    rescued=0    ignored=0   

## - then run the curl command to see welcome page
